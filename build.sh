#!/usr/bin/env bash

find src/ -name '*.java' > SOURCEFILES
source scripts/setclasspath.sh
javac -d build/ @SOURCEFILES
cd build; jar cvf ../lib/clearparser-0.3.jar . 
