package clear.engine;



import java.io.File;
import java.io.PrintStream;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.w3c.dom.Element;

import clear.dep.DepLib;
import clear.dep.DepNode;
import clear.dep.DepTree;
import clear.helper.POSTagger;
import clear.helper.Tokenizer;
import clear.reader.AbstractReader;
import clear.reader.CoNLLXReader;
import clear.reader.DepReader;
import clear.reader.PosReader;
import clear.reader.RawReader;
import clear.util.IOUtil;

/**
 * Predicts POS tags
 * @author Andrew MacKinlay
 * <b>Last update:</b> 2012-04-13
 */
public class PosPredict extends AbstractCommon
{
	@Option(name="-i", usage="input file", required=true, metaVar="REQUIRED")
	private String s_inputPath  = null;
	@Option(name="-o", usage="output file", required=true, metaVar="REQUIRED")
	private String s_outputFile = null;
	/** Tokenizing modelFile */
	private Tokenizer  g_tokenizer  = null;
	/** Part-of-speech tagging modelFile */
	private POSTagger  g_postagger   = null;
	
	static String FMT_OVERRIDE_POSTAG = ".pos-retag"; // if we see this suffix, we assume that the format is pos-tagged
		
	public PosPredict(String[] args)
	{
		CmdLineParser cmd = new CmdLineParser(this);
		
		try
		{
			cmd.parseArgument(args);
			init();
			printConfig();
						
			File file = new File(s_inputPath);
			
			if (file.isFile())
				predict(s_inputPath, s_outputFile);
			else
			{
				for (String inputFile : file.list())
				{
					inputFile = s_inputPath + File.separator + inputFile;
					predict(inputFile, inputFile+".postag");
				}
			}
			
		}
		catch (CmdLineException e)
		{
			System.err.println(e.getMessage());
			cmd.printUsage(System.err);
		}
		catch (Exception e) {e.printStackTrace();}
	}
	
	public void predict(String inputFile, String outputFile) throws Exception
	{
		AbstractReader<DepNode, DepTree> reader = null;
		
		if (inputFile.endsWith(FMT_OVERRIDE_POSTAG)) {
			reader = new PosReader(inputFile);
			System.err.println("Overriding data format as pre-tagged due to suffix " + FMT_OVERRIDE_POSTAG);
		}
		else if (s_format.equals(AbstractReader.FORMAT_RAW))	reader = new RawReader   (inputFile, g_tokenizer);
		else if (s_format.equals(AbstractReader.FORMAT_POS))	reader = new PosReader   (inputFile);
		else if (s_format.equals(AbstractReader.FORMAT_DEP))	reader = new DepReader   (inputFile, false);
		else if (s_format.equals(AbstractReader.FORMAT_CONLLX))	reader = new CoNLLXReader(inputFile, false);
		
		reader.setLanguage(s_language);
		
		PrintStream fout  = IOUtil.createPrintFileStream(outputFile);
		DepTree     tree;
		
		System.out.println("\n* Predict: "+inputFile);

		while (true)
		{
			tree = reader.nextTree();
			if (tree == null)	break;
			
			g_postagger.postag(tree);
			for (DepNode node : tree) {
				if (node.form.equals(DepLib.ROOT_TAG))
					continue; // ignore root nodes - they're not tokens
				fout.println(node.form + "\t" + node.pos);
			}
			fout.println();
			
		}
		
		fout.close();
		reader.close();
	}
	
	protected void initElements()
	{
		if (s_format.equals(AbstractReader.FORMAT_RAW) || s_format.equals(AbstractReader.FORMAT_POS))
		{
			Element ePredict = getElement(e_config, TAG_PREDICT);
			Element element;
			
			if ((element = getElement(ePredict, TAG_PREDICT_TOK_MODEL)) != null)
				g_tokenizer = new Tokenizer(element.getTextContent().trim());
			
			if ((element = getElement(ePredict, TAG_PREDICT_POS_MODEL)) != null)
				g_postagger = new POSTagger(element.getTextContent().trim());
			
		}
	}
	
	protected void printConfig()
	{
		System.out.println("* Configurations");
		System.out.println("- language   : "+s_language);
		System.out.println("- format     : "+s_format);
		System.out.println("- input_file : "+s_inputPath);
		System.out.println("- output_file: "+s_outputFile);
	}
			
	static public void main(String[] args)
	{
		new PosPredict(args);
	}
}
