A minor fork of [ClearParser](https://code.google.com/p/clearparser/) which adds support for POS-tagging in a separate phase.

There are very few changes, so the original documentation should just work the same. There is just an extra class which does POS prediction standalone.

This fork was created by Andrew MacKinlay from [NICTA](http://nicta.com.au)
for the [StructuRX](http://opennicta.com/home/structurx) package. Build using

    $ ./build.sh

(Or you might have to do it manually - this is not thoroughly tested)

This project is licensed under a New BSD (3-clause) license.
