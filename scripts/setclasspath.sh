#!/bin/sh
if [ -z $BASH_SOURCE ] ; then
	echo "Script must be sourced (using '.' or 'source') and run under bash >= 3.0"
	exit 1
fi
script_path="${BASH_SOURCE%/*}"
script_path_abs="$(cd $script_path/.. ; pwd)"
if [ -z "$CLEARPATH" ] ; then
	export CLEARPATH="$script_path_abs"
fi
jar_path="$CLEARPATH/lib"

for jarfile in "$jar_path"/*.jar ; do
	if [ -z "$CLASSPATH" ] ; then
		CLASSPATH="$jarfile"
	else
		CLASSPATH="$jarfile:$CLASSPATH"
	fi
done
if [ -d $CLEARPATH/build ] ; then
	CLASSPATH="$CLEARPATH/build:$CLASSPATH"
fi
echo "ClearParser base: $CLEARPATH; CLASSPATH is now $CLASSPATH"
export CLASSPATH

